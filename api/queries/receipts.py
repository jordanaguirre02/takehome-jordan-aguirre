from generator.id import generate_id
from generator.points import AwardPoints


# Receipt "Database" holds all queries that will be made with each request
class ReceiptQueries:
    # "Database" dictionary using the ID as key and the receipt as it's value
    receipts_data = {}

    # Converts receipt into a dictionary, generates a unique ID
    # Then saves to "Database", and return the ID
    def process_receipt(self, ReceiptIn):
        receipt = ReceiptIn.model_dump()
        receipt_id = generate_id()
        self.receipts_data[receipt_id] = receipt
        return {"id": receipt_id}

    # Checks "Database" if receipt exists by checking ID, if it does
    # Tallys points for receipt or returns error message if receipt does not
    def get_points(self, receipt_id):
        try:
            receipt = self.receipts_data[receipt_id]
        except KeyError:
            return {"detail": "Invalid Receipt ID"}
        point_checker = AwardPoints()
        points = point_checker.generate_points(receipt)
        return {"points": points}
