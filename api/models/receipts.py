from pydantic import BaseModel
from typing import List
from datetime import date, time


# Individual item model for items on receipt
class ItemModel(BaseModel):
    shortDescription: str
    price: str


# Main receipt model, to ensure an accurate receipt is sent in POST request
class ReceiptIn(BaseModel):
    retailer: str
    purchaseDate: date
    purchaseTime: time
    total: str
    items: List[ItemModel]


# ID model for response documentation
class IDOut(BaseModel):
    id: str


# Points model for response documentation
class PointsOut(BaseModel):
    points: int
