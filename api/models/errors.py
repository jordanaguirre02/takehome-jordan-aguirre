from pydantic import BaseModel


# Generic Error model, to let fastAPI know how to display errors
class DetailError(BaseModel):
    detail: str
