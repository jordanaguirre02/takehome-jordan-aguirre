# Jordan Aguirre - Process Receipts

## To get started:
1. Run Docker Compose
    - ```docker-compose up```
2. Open up FastAPI in browser
    - ```http://localhost:8000/docs#/default```
    - ![alt text](readme_images/FastAPI_Landing.png)
3. Copy sample receipt data
    - ![alt text](readme_images/Copy_Data.png)
4. On POST endpoint, click try out and paste receipt into request body
    - ![alt text](readme_images/Try_Out_Endpoint.png)
    - ![alt text](readme_images/Post_Execute.png)
    - ![alt text](readme_images/Post_Response.png)
5. On GET endpoint, paste id into parameter field
    - ![alt text](readme_images/Get_Endpoint.png)
    - ![alt text](readme_images/Get_Response.png)


## To Test with pytest:
1. Open docker bash terminal
    - ![alt text](readme_images/Docker_Terminal.png)
2. Install requirements pytest and httpx
    - ```pip install pytest```
    - ![alt text](readme_images/Install_Pytest.png)
    - ```pip install httpx```
    - ![alt text](readme_images/Install_httpx.png)
3. Run pytests
    - ```python -m pytest```
    - ![alt text](readme_images/Run_Pytest.png)
