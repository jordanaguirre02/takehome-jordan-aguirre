from fastapi.testclient import TestClient
from main import app
from queries.receipts import ReceiptQueries


client = TestClient(app)


# Fake queries that will replace the real Queries for CI/CD testing purposes
class FakeReceiptQueries:
    receipts_data = {}
    generated_id = "777"

    def process_receipt(self, ReceiptIn):
        receipt = ReceiptIn.model_dump()
        receipt_id = self.generated_id
        self.receipts_data[receipt_id] = receipt
        return {"id": receipt_id}


# Test a functional receipt input to ensure ID wil get generated
def test_process_receipts():
    app.dependency_overrides[ReceiptQueries] = FakeReceiptQueries

    ReceiptIn = {
        "retailer": "Target",
        "purchaseDate": "2022-01-02",
        "purchaseTime": "13:13",
        "total": "1.25",
        "items": [
            {"shortDescription": "Pepsi - 12-oz", "price": "1.25"}
        ]
    }

    response = client.post("/receipts/process", json=ReceiptIn)
    data = response.json()
    assert response.status_code == 200
    assert data["id"] == "777"

    app.dependency_overrides = {}


# Test a bad receipt format to ensure API will not process the data
def test_bad_receipts():
    app.dependency_overrides[ReceiptQueries] = FakeReceiptQueries

    ReceiptIn = {
        "retailer": "Target",
        "total": "1.25",
        "items": [
            {"shortDescription": "Pepsi - 12-oz", "price": "1.25"}
        ]
    }

    response = client.post("/receipts/process", json=ReceiptIn)
    assert response.status_code == 422

    app.dependency_overrides = {}
