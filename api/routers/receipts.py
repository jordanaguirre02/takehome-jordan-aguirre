from fastapi import APIRouter, Depends
from typing import Union
from models.receipts import ReceiptIn, IDOut, PointsOut
from models.errors import DetailError
from queries.receipts import ReceiptQueries

router = APIRouter()


# POST route for receipt API, generates and returns an ID
# Saves receipt to a dictionary "database".
@router.post("/receipts/process", response_model=Union[IDOut, DetailError])
def process_receipt(
    ReceiptIn: ReceiptIn,
    queries: ReceiptQueries = Depends()
):
    return queries.process_receipt(ReceiptIn)


# GET route for receipt API, looks up if receipt exists in "database"
# Returns calculated points if receipt exists, or returns error if it does not
@router.get(
    "/receipts/{receipt_id}/points",
    response_model=Union[PointsOut, DetailError]
)
def get_points(
    receipt_id: str,
    queries: ReceiptQueries = Depends()
):
    return queries.get_points(receipt_id)
