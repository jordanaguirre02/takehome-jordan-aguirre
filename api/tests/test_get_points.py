from fastapi.testclient import TestClient
from main import app
import datetime
from generator.points import AwardPoints
from queries.receipts import ReceiptQueries


client = TestClient(app)


# Fake queries that will replace the real Queries for CI/CD testing purposes
class FakeReceiptQueries:
    receipts_data = {"777": {
        "retailer": "Walgreens",
        "purchaseDate": datetime.date(2022, 1, 2),
        "purchaseTime": datetime.time(13, 13),
        "total": "2.65",
        "items": [
            {"shortDescription": "Pepsi - 12-oz", "price": "1.25"},
            {"shortDescription": "Dasani", "price": "1.40"}
        ]
    }}

    def get_points(self, receipt_id):
        try:
            receipt = self.receipts_data[receipt_id]
        except KeyError:
            return {"detail": "Invalid Receipt ID"}
        point_checker = AwardPoints()
        points = point_checker.generate_points(receipt)
        return {"points": points}


# Test a known ID in "database" to verify it will find receipt
# Calculate points accurately
def test_get_points():
    app.dependency_overrides[ReceiptQueries] = FakeReceiptQueries

    response = client.get("/receipts/777/points")
    data = response.json()

    assert response.status_code == 200
    assert data == {"points": 15}
    app.dependency_overrides = {}


# Tests a bad ID to verify it will product an error
# If the receipt does not exist
def test_bad_get_points():
    app.dependency_overrides[ReceiptQueries] = FakeReceiptQueries

    response = client.get("/receipts/789/points")
    data = response.json()

    assert response.status_code == 200
    assert data == {"detail": "Invalid Receipt ID"}

    app.dependency_overrides = {}
