from math import ceil


# Point checking class, separated by each point category
class AwardPoints:
    def __init__(self):
        self.retailer_points = 1
        self.even_total_points = 50
        self.quarter_multiple_total_points = 25
        self.two_items_points = 5
        self.description_points = 0.2
        self.odd_date_points = 6
        self.power_hour_points = 10

    def retailer_check(self, name):
        points = 0
        name_list = list(name)
        for char in name_list:
            if char.isalnum():
                points += self.retailer_points
        return points

    def even_total_check(self, total):
        points = 0
        total = float(total)
        if int(total) == total:
            points += self.even_total_points
        return points

    def quarter_multiple_check(self, total):
        points = 0
        total = float(total)
        if total % 0.25 == 0:
            points += self.quarter_multiple_total_points
        return points

    def two_item_check(self, items):
        points = 0
        number_of_item_pairs = len(items) // 2
        points += (number_of_item_pairs * self.two_items_points)
        return points

    def item_description_check(self, items):
        points = 0
        for item in items:
            item_length = len(item["shortDescription"].strip())
            item_price = float(item["price"])
            if item_length % 3 == 0:
                points += ceil(item_price * self.description_points)
        return points

    def odd_day_check(self, purchase_date):
        points = 0
        day = purchase_date.day
        if int(day) % 2 == 1:
            points += self.odd_date_points
        return points

    def power_hour_check(self, purchase_time):
        points = 0
        hour = purchase_time.hour
        if hour == 14:
            minute = purchase_time.minute
            if minute > 0:
                points += self.power_hour_points
        elif hour > 14 and hour < 16:
            points += self.power_hour_points
        return points

    # Main function to utilize that will call all checks and return point total
    def generate_points(self, receipt):
        points = 0
        points += self.retailer_check(receipt["retailer"])
        points += self.even_total_check(receipt["total"])
        points += self.quarter_multiple_check(receipt["total"])
        points += self.two_item_check(receipt["items"])
        points += self.item_description_check(receipt["items"])
        points += self.odd_day_check(receipt["purchaseDate"])
        points += self.power_hour_check(receipt["purchaseTime"])
        return points
