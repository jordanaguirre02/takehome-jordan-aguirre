import uuid


# Generate a unique receipt id using UUID
def generate_id():
    generated_id = str(uuid.uuid1())
    return generated_id
